import { createRouter, createWebHistory } from "vue-router";
import HomePage from "@/pages/HomePage";



const ifNotAuthorized = (to, from, next) => {
    if (localStorage.getItem('token')) {
        next("/");
    } else {
        next();
    }
};

const ifAuthorized = (to, from, next) => {
    if (localStorage.getItem('token')) {
        next();
    } else {
        next("/sign-in");
    }
};

const routes = [
    {
        path: "/",
        component: HomePage,
        beforeEnter: ifAuthorized,
    },
    {
        path: "/cart",
        component: () => import('../pages/CartPage.vue'),
        beforeEnter: ifAuthorized,
    },
    {
        path: "/sign-in",
        component: () => import("../pages/SignInPage.vue"),
        beforeEnter: ifNotAuthorized
    },
    {
        path: "/sign-up",
        component: () => import("../pages/SignUpPage.vue"),
        beforeEnter: ifNotAuthorized,
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});



export default router;








// import {createRouter, createWebHistory} from "vue-router"
// import HomePage from "@/pages/HomePage";
// import store from "@/plugins/vuex/store";
//
//
//
// const ifNotAuthorized = (to, from, next) => {
//     if(store.getters.isAuthorized) {
//         next('/')
//     } else {
//         next()
//     }
// }
//
// const ifAuthorized = (to, from, next) => {
//     if(store.getters.isAuthorized) {
//         next()
//     } else {
//         next('/sign-in')
//     }
// }
//
//
//
// const routes = [
//     {
//         path: "/",
//         component: HomePage,
//         beforeEnter: ifAuthorized
//     },
//     {
//         path: '/sign-in',
//         component: () => import('../pages/SignInPage.vue'),
//         beforeEnter: ifNotAuthorized
//     },
//     {
//         path: '/sign-up',
//         component: () => import('../pages/SignUpPage.vue'),
//         beforeEnter: ifNotAuthorized
//     }
// ];
//
//
// store.watch(
//     () => store.getters.getToken,
//     (newValue) => {
//         if (newValue) {
//             router.beforeEach((to, from, next) => ifAuthorized(to, from, next));
//         } else {
//             router.beforeEach((to, from, next) => ifNotAuthorized(to, from, next));
//         }
//     }
// );
//
//
// export default createRouter({
//     history: createWebHistory(),
//     routes,
// })