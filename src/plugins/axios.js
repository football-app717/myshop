import axios from "axios"
import store from "@/plugins/vuex/store"

axios.defaults.baseURL = process.env.VUE_APP_API_URL + '/'


axios.interceptors.request.use((config) => {
    if (config.url !== '/users/auth') {
        config.headers.authorization = 'bearer ' + store.getters.getToken
    }

    return config
})

export default axios