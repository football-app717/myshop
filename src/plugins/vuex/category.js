import axios from "../axios";


export default {
    actions: {
        fetchCategories(context) {
            return new Promise((resolve, reject) => {
                axios
                    .get("/categories")
                    .then((response) => {
                        context.commit("updateCategories", response.data);
                        resolve();
                    })
                    .catch((err) => {
                        reject(err.response.data);
                    });
            });
        },

    },
    mutations: {
        updateCategories(state, categories) {
            state.categories = categories;
        },
    },
    state: {
        categories: {}
    },
    getters: {
        getCategories(state) {
            return state.categories;
        },
    }
}