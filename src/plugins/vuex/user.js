import axios from "../axios";


export default {
    actions: {
        fetchToken(context, data) {
            return new Promise((resolve, reject) => {
                axios
                    .post("/users/auth", data)
                    .then((response) => {
                        context.commit("updateUser", response.data);
                        resolve();
                    })
                    .catch((err) => {
                        reject(err.response.data);
                    });
            });
        },
        pushUser(context, data) {
            return new Promise((resolve, reject) => {
                axios
                    .post("/users", data)
                    .then((response) => {
                        console.log("Hammasi joyida user qoshildi");
                        console.log(response);
                        context.commit('updateUser', response.data);
                        resolve();
                    })
                    .catch((err) => {
                        console.log("Hattolik yuz berdi");
                        console.log(err.response.data);
                        reject(err.response.data)
                    })
            })
        },
        fetchAboutMe(context) {
            return new Promise((resolve, reject) => {
                axios
                    .get('/users/about_me')
                    .then((response) => {
                        context.commit('updateUser', response.data);
                    })
                    .catch((err) => {
                        reject(err.response.data);
                    })
            })
        }

    },
    mutations: {
        updateUser(state, user) {
            localStorage.setItem("token", user.token)

            state.token = user.token;
            state.user = user;
        },
        updateToken(state, token) {
            state.token = token;
        }
    },
    state: {
        token: localStorage.getItem('token'),
        user: {
            _id: null,
            name: null,
            sname: null,
            email: null,
            createdAt: null,
            updatedAt: null,
        }
    },
    getters: {
        getToken(state) {
            return state.token;
        },
        getUser(state) {
            return state.user;
        },
        isAuthorized(state) {
            return state.token !== null
        },
    }
}