
export default {
    actions: {
        pushItems(context, data) {
           context.commit('updateItems', data)
        },
        removeItem(context, itemId) {
            context.commit('deleteItem', itemId);
        },
        incrementItem(context, itemId) {
            context.commit('incrementItemCount', itemId);
        },
        decrementItem(context, itemId) {
            context.commit('decrementItemCount', itemId);
        }
    },
    mutations: {
        updateItems(state, items) {
            const existingItem = state.items.find(item => item._id === items._id);
            if (existingItem) {
                existingItem.count += 1;
            } else {
                state.items.push({...items, count: 1});
            }
        },
        deleteItem(state, itemId) {
            const itemIndex = state.items.findIndex(item => item._id === itemId);
            if (itemIndex !== -1) {
                if (state.items[itemIndex].count > 1) {
                    state.items[itemIndex].count -= 1;
                } else {
                    state.items.splice(itemIndex, 1);
                }
            }
        },
        incrementItemCount(state, itemId) {
            const item = state.items.find(item => item._id === itemId);
            if (item) {
                item.count += 1;
            }
        },
        decrementItemCount(state, itemId) {
            const itemIndex = state.items.findIndex(item => item._id === itemId);
            if (itemIndex !== -1) {
                if (state.items[itemIndex].count > 1) {
                    state.items[itemIndex].count -= 1;
                } else {
                    state.items.splice(itemIndex, 1);
                }
            }
        }
    },
    state: {
        items: []
    },
    getters: {
        getItems(state) {
            return state.items;
        },
        getTotalItems(state) {
            return state.items.reduce((total, item) => total + item.count, 0);
        },
        getTotalPrice(state) {
            return state.items.reduce((total, item) => total + (item.price * item.count), 0);
        },
    }
}