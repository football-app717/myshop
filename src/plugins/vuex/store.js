import {createStore} from "vuex";
import user from "@/plugins/vuex/user";
import category from "@/plugins/vuex/category";
import product from "@/plugins/vuex/product";
import shop from "@/plugins/vuex/shop";
export default createStore({
    modules: {
        user,
        category,
        product,
        shop
    }
});