import axios from "../axios";


export default {
    actions: {
        fetchProducts(context) {
            return new Promise((resolve, reject) => {
                axios
                    .get("/products")
                    .then((response) => {
                        console.log(response.data)
                        context.commit("updateProducts", response.data);
                        resolve();
                    })
                    .catch((err) => {
                        reject(err.response.data);
                    });
            });
        },

    },
    mutations: {
        updateProducts(state, products) {
            state.products = products;
        },
    },
    state: {
        products: {}
    },
    getters: {
        getProducts(state) {
            return state.products;
        },
    }
}